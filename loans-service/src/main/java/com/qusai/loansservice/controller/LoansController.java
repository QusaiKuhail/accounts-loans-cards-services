package com.qusai.loansservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.qusai.loansservice.model.Loans;
import com.qusai.loansservice.repository.LoansRepository;

@RestController
public class LoansController {
	
	@Autowired
	private LoansRepository loansRepository;

	@GetMapping("/myLoans")
	public List<Loans> getLoansDetails(@RequestParam int customerId) {
		List<Loans> loans = loansRepository.findByCustomerIdOrderByStartDtDesc(customerId);
		if (loans != null) {
			return loans;
		} else {
			return null;
		}

	}

}
