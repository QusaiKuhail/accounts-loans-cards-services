# API Documentation

This repository contains a set of RESTful APIs built using Java Spring Boot. These APIs provide information about a customer's accounts, loans, and cards. Below are the available endpoints and their descriptions:

## Customer's Accounts

### Endpoint
- URL: `http://localhost:8080/myAccount`
- HTTP Method: GET

### Request Parameters
- `customerId` (required): The unique identifier of the customer.

### Description
This endpoint allows you to retrieve information about a customer's accounts.

## Customer's Loans

### Endpoint
- URL: `http://localhost:8090/myLoans`
- HTTP Method: GET

### Request Parameters
- `customerId` (required): The unique identifier of the customer.

### Description
This endpoint allows you to retrieve information about a customer's loans.

## Customer's Cards

### Endpoint
- URL: `http://localhost:9000/myCards`
- HTTP Method: GET

### Request Parameters
- `customerId` (required): The unique identifier of the customer.

### Description
This endpoint allows you to retrieve information about a customer's cards.

## Getting Started

To use these APIs, follow these steps:

1. Clone this repository to your local machine:

   ```shell
   git clone https://gitlab.com/QusaiKuhail/accounts-loans-cards-services.git
