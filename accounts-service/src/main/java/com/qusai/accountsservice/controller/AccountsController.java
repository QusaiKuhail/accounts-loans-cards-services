/**
 * 
 */
package com.qusai.accountsservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.qusai.accountsservice.model.Accounts;
import com.qusai.accountsservice.repository.AccountsRepository;

@RestController
public class AccountsController {

	@Autowired
	private AccountsRepository accountsRepository;

	@GetMapping("/myAccount")
	public Accounts getAccountDetails(@RequestParam int customerId) {

		Accounts accounts = accountsRepository.findByCustomerId(customerId);
		if (accounts != null) {
			return accounts;
		} else {
			return null;
		}

	}

}