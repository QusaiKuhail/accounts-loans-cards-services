package com.qusai.cardsservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.qusai.cardsservice.model.Cards;
import com.qusai.cardsservice.repository.CardsRepository;

@RestController
public class CardsController {
	@Autowired
	private CardsRepository cardsRepository;
	
	@GetMapping("/myCards")
	public List<Cards> getCardDetails(@RequestParam  int customerId) {
		List<Cards> cards = cardsRepository.findByCustomerId(customerId);
		if (cards != null) {
			return cards;
		} else {
			return null;
		}

	}

}
